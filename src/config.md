
## Create your own config.yaml using below structure 

content:
  filename: "/usr/local/speech/src/original_text/step_content.csv"

s3_config:
  s3_source_bucket: "**********"
  access_key: "**********"
  secret_key: "**********"
  s3_region: "**********"
  input_file_path: "**********"
  output_file_path: "**********"
 

mysql_server:
  host: "0.0.,0.0"
  port: 3308
  database: "timesheet"
  username: "myadmin"
  password: "password123"

