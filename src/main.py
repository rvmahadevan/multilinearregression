from typing import Optional
from fastapi import FastAPI
from fastapi.responses import JSONResponse
import uvicorn
import json
import pandas as pd
import io
import yaml
import boto3
import json
from fastapi.responses import HTMLResponse
# from nltk.tokenize.regexp import WordPunctTokenizer, WhitespaceTokenizer

app = FastAPI()

param = open("config.yaml")
parsed_param = yaml.load(param, Loader=yaml.FullLoader)

s3_source_bucket = parsed_param["s3_config"]["s3_source_bucket"]
access_key = parsed_param["s3_config"]["access_key"]
secret_key = parsed_param["s3_config"]["secret_key"]
s3_region = parsed_param["s3_config"]["s3_region"]
s3_input_path = parsed_param["s3_config"]["input_file_path"]
s3_output_path = parsed_param["s3_config"]["output_file_path"]


s3_client = boto3.client(
    service_name='s3',
    region_name=s3_region,
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key
)

@app.get("/")
def read_root():
    return {"Hello": "Welcome to Docker Learning"}


@app.get("/test/{num1}/{num2}")
async def test(num1: int, num2: int):
    output = num1 + num2
    return output

@app.get("/reads3/{bucket}/{filepath}")
def read_data_s3(bucket: str, filepath: str):
    obj = s3_client.get_object(Bucket=bucket, Key=filepath)
    data = obj['Body'].read()
    mydata = pd.read_csv(io.BytesIO(data))
    result = mydata.to_json(orient="records")
    # parsed = json.loads(result)
    # ds = json.dumps(parsed, indent=4) 
    return result

@app.get("/list_buckets")
async def list_buckets():
    buckets = s3_client.list_buckets()
    return buckets.to_json(orient="records")

@app.get("/s3test")
def s3test():
    obj = s3_client.get_object(Bucket=s3_source_bucket, Key='training/sales.csv')
    data = obj['Body'].read()
    mydata = pd.read_csv(io.BytesIO(data))
    result = mydata.to_json(orient="records")
    # parsed = json.loads(result)
    # ds = json.dumps(parsed, indent=4) 
    return result

@app.get("/home")
async def home():
    content = """
                <body>
                <body>

                    <h1>Welcome to Our Company</h1>
                    <h2>Web Site Main Ingredients:</h2>

                    <p>Pages (HTML)</p>
                    <p>Style Sheets (CSS)</p>
                    <p>Computer Code (JavaScript)</p>
                    <p>Live Data (Files and Databases)</p>

                    </body>
                </body>
            """
    return HTMLResponse(content=content)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5000, debug=True)

