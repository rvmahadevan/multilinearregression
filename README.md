# Get Started

## Pre-requesites

* Docker : Knowledge in running docker.
* Experience in MYSQL or any relational databases.
* Python 3


## Start Application

* Clone the repository from git store.
* Navigate to flask-timesheet folder.
| Run >$ docker-compose -f docker-composer.yml up --build

Run >$ docker-compose -f docker-composer.yml up --build -d in (daemon mode)

On successfull start go to localhost:9090 to see application login page


