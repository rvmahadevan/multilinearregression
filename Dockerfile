FROM python:3.6
LABEL NAME: Mahadevan Varadhan

# USER root
# RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
# RUN unzip awscliv2.zip
# RUN sudo ./aws/install

# USER devadmin

# Create app directory
WORKDIR /usr/local/myapp/

# Install app dependencies
COPY /src/ /usr/local/myapp/src/
COPY requirements.txt ./
# COPY /config/ /usr/local/myapp/config/

RUN pip install --upgrade pip

# RUN python -m pip install grpcio
# RUN python -m pip install grpcio-tools

# RUN pip install --upgrade --no-cache-dir -r requirements.txt

RUN pip install -U Flask && \
    pip install Flask-RESTful && \
    pip install Flask-JWT && \
    pip install flask-bcrypt && \
    pip install Flask-Migrate && \
    pip install flask_login && \
    pip install pandas && \
    pip install nltk && \
    pip install sqlalchemy && \
    pip install psycopg2-binary && \
    pip install --upgrade werkzeug  && \
    pip install pyyaml  && \
    pip install pymysql && \
    pip install cryptography && \
    pip install simplejson  && \
    pip install jupyter && \
    pip install SpeechRecognition && \
    pip install google-cloud-texttospeech && \
    pip install pillow && \
    pip install scikit-image && \
    pip install scikit-learn && \
    pip install scipy && \
    pip install six && \
    pip install sklearn && \
    pip install torch && \
    # pip install librosa && \
    pip install boto3 && \
    pip install redis && \
    pip install fastapi && \
    pip install uvicorn && \
    pip install pydub && \
    pip install opencv-python 
   
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y && \
    apt-get -y install apt-utils gcc libpq-dev libsndfile-dev 

# ENTRYPOINT ["python"]
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]

EXPOSE 5000 8080 6379 8888 
